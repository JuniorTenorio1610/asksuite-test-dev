const BrowserService = require('../services/BrowserService');
const moment = require('moment');  

class SearchController {
    static async getRoomHotel(checkin, checkout) {
        checkin = (moment(checkin).format('DDMMYYYY'));
        checkout = (moment(checkout).format('DDMMYYYY'));
        const browser = await BrowserService.getBrowser();
        const result = await BrowserService.searchRoom(checkin, checkout, browser);
        await BrowserService.closeBrowser();
        return result;
    }
}

module.exports = SearchController;