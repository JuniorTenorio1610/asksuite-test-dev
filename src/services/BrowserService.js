const puppeteer = require('puppeteer');
const BuildUrl = require('../utils/Util');

class BrowserService {

    static async getBrowser() {
        return puppeteer.launch({});
    }

    static closeBrowser(browser) {
        if (!browser) {
            return;
        }
        return browser.close();
    }

    static async searchRoom(checkin, checkout, browser) {
        let rooms = [];
        try{
            const page = await browser.newPage();
            const url = await BuildUrl.buildUrlSearch('hotelresults', checkin, checkout);
            await page.goto(url, {waitUntil: 'load', timeout: 0});
            await page.waitForSelector('.hotel_name')
            let elements = await page.$$('.roomrateinfo');
            rooms = await this.serializeTextElement(elements, page);
            rooms = await BuildUrl.mergeInfoRoom(rooms);
            let descriptionDiv = await page.$$('.hotel-description');
            rooms = await this.margeDescription(rooms, descriptionDiv, page);
        } catch (err) {
            console.error(`'Puppeteer Error Detencted -> ${err}'`);
            throw new Error(err);
        }
        return rooms;
    }
    
    static async serializeTextElement(elements, page){
        let rooms = [];
        for (const [key, el] of elements.entries()) {
            let nameRoom = await page.evaluate(el => el.getAttribute("data-name"), el);
            let priceRoom = await page.evaluate(el => el.getAttribute("data-price"), el);
            let idRoom = await page.evaluate(el => el.getAttribute("data-room-id"), el);
            let imgRoom = await page.$$eval('img[data-target=".modal-room-'+idRoom+'"]', imgs => imgs.map(img => img.getAttribute('src')));
            
            rooms.push({
                'idOMN': idRoom,
                'name' : nameRoom.replace(/(\r\n|\n|\r)/gm, ""),
                'description' : '',
                'price' : priceRoom,
                'image' : imgRoom.shift()
            });
        
        }
        return rooms
    }

    static async margeDescription(rooms, description, page) {
        for(const [key, el] of rooms.entries()){
            let descriptionRoom = await page.evaluate(el => el.textContent, description[key]);    
            descriptionRoom = descriptionRoom.replace(' (ver mais)', "");
            el.description = descriptionRoom.replace(/(\r\n|\n|\r)/gm, "");
            delete el.idOMN;
        }
        return rooms;
    }
}

module.exports = BrowserService;
