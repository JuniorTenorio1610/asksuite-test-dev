const fs = require('fs');
const axios = require('axios');
const server = require('..//server');
const request = require('supertest');
const mockResponse = require('./response.json');
jest.mock("axios");

describe('Testando as rotas', () => {
   test('Testando a rota / ', async () => {
      const res = await request(server).get('/');
      expect(res.statusCode).toEqual(200);
      expect(res.text).toEqual('Hello Asksuite World!');
   });

   test('Testando a rota /search sucesso', async () => {
      const res = await request(server).post('/search').send({
        "checkin": "2021-09-21", 
         "checkout": "2021-09-24"
      });
      expect(res.text).toBe(JSON.stringify(mockResponse));
      expect(res.statusCode).toEqual(200);
   }, 50000);

   test('Testando a rota /search erro com o parametro checkin', async () => {
      const res = await request(server).post('/search').send({
            "data-entrada": "2021-09-21", 
            "checkout": "2021-09-24"
      });

      expect(res.text).toEqual('{"errors":[{"msg":"O campo checkin é obrigatório","param":"checkin","location":"body"}]}');
      expect(res.statusCode).toEqual(400);
   });

   test('Testando a rota /search erro com o parametro checkout', async () => {
      const res = await request(server).post('/search').send({
            "checkin": "2021-09-21", 
            "data-saida": "2021-09-24"
      });

      expect(res.text).toEqual('{"errors":[{"msg":"O campo checkout é obrigatório","param":"checkout","location":"body"}]}');
      expect(res.statusCode).toEqual(400);
   });

   test('Testando a rota /search erro com os parametros checkin/checkout', async () => {
      const res = await request(server).post('/search').send({
            "data-entrada": "2021-09-21", 
            "data-saida": "2021-09-24"
      });

      expect(res.text).toEqual('{"errors":[{"msg":"O campo checkin é obrigatório","param":"checkin","location":"body"},{"msg":"O campo checkout é obrigatório","param":"checkout","location":"body"}]}');
      expect(res.statusCode).toEqual(400);
   });
});

