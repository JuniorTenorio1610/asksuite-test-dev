const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const SearchController = require('../controller/SearchController');

router.get('/', (req, res) => {
    res.send('Hello Asksuite World!');
});

router.post('/search',[
    body('checkin').notEmpty().withMessage("O campo checkin é obrigatório"),
    body('checkout').notEmpty().withMessage("O campo checkout é obrigatório")
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const checkin = req.body.checkin;
    const checkout = req.body.checkout;
    try{
        const response = await SearchController.getRoomHotel(checkin, checkout);
        if (response.length === 0) {
            res.statusCode = 404;
            res.send('Nenhum quarto foi encontrado.');
        }
        res.send(response);
    } catch (err) {
        res.statusCode = 500;
        res.send('Erro interno ao processar esta requisição.');
    }
});


module.exports = router;
