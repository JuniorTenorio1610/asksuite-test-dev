require('dotenv').config();

const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
});

class Util {
    static async buildUrlSearch(path, checkin, checkout){
        const paramsDate = '?CheckIn='+checkin+'&CheckOut='+checkout;
        const url = process.env.URL_BASE_OMN+path+paramsDate+process.env.PARAMS_REQUIRE;

        return url;
    }

    static async mergeInfoRoom(room){
        let res = room.reduce((m, o) => {
            let found = m.find(p => p.idOMN === o.idOMN);
            if (found) {
                if(Number(o.price) < Number(found.price)){
                    console.log(found.price);
                    found.price = o.price;
                    found.name = o.name;
                    found.img = o.img;
                }
            } else {
                o.price = formatter.format(o.price).replace(/\s/g, '');
                m.push(o);
            }
            return m;
        }, []);;
        return res;
    }
}

module.exports = Util;