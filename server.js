require('dotenv').config();
const express = require('express');
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const router = require('./src/routes/router.js');

const app = express();
app.use(express.json());

const port = process.env.PORT;

app.use('/', router);
app.listen(8081, () => {
    console.log(`Listening on port ${port}`);
});

module.exports = app;